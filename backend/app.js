var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var cfenv = require("cfenv")

var index = require('./routes/index');
var api = require('./routes/api');

var app = express();

var local_vcap = {
  services: {
    mongodb: [
        {        
          name: "mongodb-76b24",
          label: "mongodb",
          tags: [
            "nosql",
            "document",
            "data_management",
            "ibm_experimental"
            ],
          plan: "100",
          credentials: {
              hostname: "10.0.116.49",
              host: "10.0.116.49",
              port: 10001,
              username: "be879069-b273-4656-b5fb-3daa5c508044",
              password: "f268582e-0a52-42a8-9b97-66889a9cb662",
              name: "76ea370c-8678-4c51-b3cf-a0cd722ed93a",
              db: "db",
              url: "mongodb://localhost:27017/tshare"
          }
        }
    ]
  }
};

var appEnv = cfenv.getAppEnv({vcap: local_vcap});

mongoose.connect(appEnv.services.mongodb[0].credentials.url);

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    // we're connected!
    console.log("Connected correctly to server");
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/api', api);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('URL not found: ' + req.originalUrl);
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
