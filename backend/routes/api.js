'use strict';
var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var randomstring = require("randomstring");
var request = require('request');
var GoogleURL = require( 'google-url' );

var Notes = require('../model/notes');

var router = express.Router();

const LONG_URL_BASE = "tshare.mybluemix.net"
const GOOGLE_SHORTENER_API_KEY = "AIzaSyADY4fHhTx0ldztu_YPpw3lNKpGnB8Ryhg";
var googleUrl = new GoogleURL({key: GOOGLE_SHORTENER_API_KEY});

router.use(bodyParser.json());

router.route('/note/:noteId')

.get(function(req, res, next){

  Notes.findOne({name: req.params.noteId}, 
    function(err, note){
      if(err) throw err;
      if(note){
        res.json({text: note.text, editable: note.editable, short_url: note.short_url});
      }else{
        res.json({});
      }      
    });
})

.post(function(req, res, next){

  var new_note = {
    name: req.params.noteId,
    editable: req.body.editable,
    password: req.body.password,
    text: req.body.text
  }

  postNote(new_note, res);
});

// Get not-used random name for the note
function get_free_name(cb){
  var note_name = randomstring.generate(7);

  Notes.findOne({name: note_name}, 
    function(err, note){
      if(err) throw err;
      if(note){
        cb(get_free_name(cb));
      }else{
        cb(note_name);
      }      
    });
}

router.route('/note')

.post(function(req, res, next){
  
  var new_note = {
    name: req.body.name,
    editable: req.body.editable,
    password: req.body.password,
    text: req.body.text,
    short_url: req.body.short_url    
  }

  postNote(new_note, res);  
});

function postNote(new_note, res){
  if(!new_note.editable && !new_note.password){
    res.json({error: "No password for not editable note"});
  }else{
    if(!new_note.name){
      get_free_name(function(new_name){
        new_note.name = new_name;
        saveNote(new_note, res);
      })
    }else{
      checkPasswordAndSave(new_note, res);
    }
  }
}

function saveNote(new_note, res){
  Notes.findOneAndUpdate({name: new_note.name},
    {
        $set: new_note
    }, 
    {
        new: true,
        upsert: true
    }, function (err, note) {
        if (err) throw err;
        res.json(note);
    });
}

function checkPasswordAndSave(new_note, res){

  Notes.findOne({name: new_note.name}, 
    function(err, note){
      if(err) throw err;
      if(note){        
        if( note.editable == true ||
            note.password === new_note.password){
          new_note.short_url = note.short_url;
          saveNote(new_note, res);
        }else{
          res.json({error: "Wrong password"});
        }
      }else{

        let url = LONG_URL_BASE + "/" + new_note.name;
        googleUrl.shorten( url, function( err, shortUrl ) {
          if(!err){
            new_note.short_url = shortUrl;
          }
          saveNote(new_note, res);
        } );
      }      
    });
}

module.exports = router;
