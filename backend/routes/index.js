var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var path = require('path');

var router = express.Router();

router.use(bodyParser.json());

router.route('/')
.get(function(req, res, next) {
  res.sendFile(path.join(__dirname + '/../public/index.html'));
});

router.route('/:noteId')
.get(function(req, res, next) {
  res.sendFile(path.join(__dirname + '/../public/index.html'));
});

module.exports = router;
