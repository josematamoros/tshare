// grab the things we need
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

// create a schema
var noteSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    editable: {
        type: Boolean,
        required: true
    },
    password: {
        type: String,
        required: false
    },
    text: {
        type: String,
        required: true
    },
    short_url: {
        type: String,
        required: false
    }    
}, {
    timestamps: true
});

// the schema is useless so far
// we need to create a model using it
var Notes = mongoose.model('Note', noteSchema);

// make this available to our Node applications
module.exports = Notes;