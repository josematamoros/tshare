var gulp = require('gulp'),    
    del = require('del');

var DIST_PATH = '../backend/public';

gulp.task('deploy', function(){
  return gulp.src(['app/**/*'])
    .pipe(gulp.dest(DIST_PATH));
});

// Clean
gulp.task('clean', function() {
    return del([DIST_PATH], {force: true});
});

// Default task
gulp.task('default', ['deploy']);

gulp.watch('app/**/*', ['deploy']);
