'use strict';

angular.module('tshareApp', ['ui.router', 'ngResource'])
      .config(function($stateProvider, $urlRouterProvider) {
        $stateProvider
        
            // route for editing a note
            .state('app', {
                url:'/',
                views: {
                    'header': {
                        templateUrl : 'views/header.html',
                    },
                    'content': {
                        templateUrl : 'views/note.html',
                        controller  : 'NoteEditorController'
                    },
                    'footer': {
                        templateUrl : 'views/footer.html',
                    }
                }

            })

            ;                 
    
        $urlRouterProvider.otherwise('/');
    })
;
