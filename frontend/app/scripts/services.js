'use strict';

angular.module('tshareApp')
        .constant("baseUrl", "/api/")
        .service('noteFactory', ["$resource", "baseUrl", function($resource, baseURL) {   
  
          this.getNote = function(){
              return $resource(baseURL+"note/:id",null,  {'update':{method:'POST' }});
          };

          this.getUnnamedNote = function(){
              return $resource(baseURL+"",null,  {'update':{method:'POST' }});
          };                        
        }])
;
