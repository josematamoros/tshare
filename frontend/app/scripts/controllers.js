'use strict';

angular.module('tshareApp')
        
        .controller('NoteEditorController', ['$scope', 'noteFactory', '$location', '$timeout', function($scope, noteFactory, $location, $timeout) {
            
            function getNoteId(){
                var absUrl = $location.absUrl();
                var elems = absUrl.split('/');
                var noteId = elems[elems.length - 2];
                noteId = noteId.substring(0, noteId.length - 1);

                return noteId;
            }
            var noteId = getNoteId();
            $scope.showNote = (noteId != "");
            $scope.showInit = (noteId == "");

            $scope.showHelp = false;
            $scope.toggleHelp = function(){
                if($scope.showHelp == false){
                    $scope.showHelp = true;
                }else{
                    $scope.showHelp = false;
                }
            }

            $scope.note = noteFactory.getNote().get({id: noteId}, function(){
                if($scope.note.editable == undefined){
                    $scope.note.editable = true;
                }
            });
            $scope.showError = false;
            $scope.showOk = false;

            $scope.saveNote = function(){
                noteFactory.getNote().save({
                   name: noteId,
                   text: $scope.note.text,
                   editable: $scope.note.editable,
                   password: $scope.note.password
                }, function(data){
                    if(data.error){
                        $scope.showError = true;
                        $scope.showOk = false;
                    }else{
                        $scope.showError = false;
                        $scope.showOk = true;
                        // Update the note with the content of the server, to obtain the short url
                        $scope.note = noteFactory.getNote().get({id: noteId});
                    }

                    $timeout(function(){
                        $scope.showError = false;
                        $scope.showOk = false;
                    }, 5000);
                });
            }

            $scope.goToNote = function(note_id){
                window.location = '/' + note_id;
                // console.log(note_id);
                // $location.url('/' + note_id);
            }

        }])
;
