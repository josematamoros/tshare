# README #

## Description

The idea of the service to make a copy/paste online tool with and open REST API and some improvements respect to other similar services.

An user could use this app to share information with others very quickly, allowing them to edit the information or just to read it. This service could also be useful for sharing information between devices, i.e. a mobile phone and a laptop.

## Compilation steps

1.- Prepare the frontend

    cd frontend
    npm install
    bower install
    gulp

2.- Prepare the backend

    cd backend
    npm install
    npm start

## Execution

    cd backend
    npm start
