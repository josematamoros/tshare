'use strict';

angular.module('tshare.controllers', [])
        
        .controller('NoteEditorController', ['$scope', 'noteFactory', '$cordovaToast', function($scope, noteFactory, $cordovaToast) {
            
            $scope.noteController.showError = false;
            $scope.noteController.showOk = false;

            $scope.noteController.note = undefined;
            $scope.noteController.note_loaded = false;

            function showToast(message, duration, position){
                $cordovaToast.show(message, duration, position);
            }

            $scope.openNote = function(){
                $scope.noteController.note_loaded = false;
                $scope.noteController.note = noteFactory.getNote().get({id: $scope.noteController.note_id}, function(){
                    $scope.noteController.note_loaded = true;
                    if($scope.noteController.note.editable == undefined){
                        $scope.noteController.note.editable = true;
                    }
                });
            }

            $scope.saveNote = function(){
                noteFactory.getNote().save({
                   name: $scope.noteController.note_id,
                   text: $scope.noteController.note.text,
                   editable: $scope.noteController.note.editable,
                   password: $scope.noteController.note.password
                }, function(data){
                    if(data.error){
                        $scope.noteController.showError = true;
                        $scope.noteController.showOk = false;
                        showToast("Error saving the note, check the password or the connection", 
                                            'long', 'center');
                    }else{
                        $scope.noteController.showError = false;
                        $scope.noteController.showOk = true;
                        showToast("Saved!", 'long', 'center');
                        // Update the note with the content of the server, to obtain the short url
                        $scope.noteController.note = noteFactory.getNote().get({id: $scope.noteController.note_id});
                    }

                    // $timeout(function(){
                    //     $scope.noteController.showError = false;
                    //     $scope.noteController.showOk = false;
                    // }, 5000);
                });
            }
        }])
;
