'use strict';

angular.module('tshare.services', ['ngResource'])
        .constant("baseUrl", "https://tshare.mybluemix.net/api/")
        .service('noteFactory', ["$resource", "baseUrl", function($resource, baseURL) {   
  
          this.getNote = function(){
              return $resource(baseURL+"note/:id",null,  {'update':{method:'POST' }});
          };

          this.getUnnamedNote = function(){
              return $resource(baseURL+"",null,  {'update':{method:'POST' }});
          };                        
        }])
;
